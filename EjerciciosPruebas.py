#Ejercicios:
#1.-Escribe 3 funciones que calculen la suma de los números de una lista. En la primera función utiliza un ciclo For, en la segunda un While y en la tercera recursividad.

def cicloFor():
    numElementos = int(input("Escriba el número de elementos que tendrá la lista: "))
    lista = []
    for i in range (numElementos):
        numero = int(input("Añada un número a la lista:" ))
        lista.append(numero)
    sumaElementos = sum(lista)
    print("lista de elementos: ", lista)
    print("Suma de elementos: ", sumaElementos)

cicloFor()



#Utilizando ciclo while
print("---------------------Utilizando ciclo while-----------------------")

def cicloWhile():
    lista = []
    num = int(input("Añada un número a la lista (Escriba 0 para dejar de agregar): "))
    while num != 0:
        lista.append (num)
        num = int ( input ( "Añada el siguiente número a la lista: " ) )

    sumaElementos = sum(lista)
    print("lista de elementos: ", lista)
    print("Suma de elementos: ", sumaElementos)
cicloWhile()




#Utilizando recusrividad
print("---------------------Utilizando Recursividad-----------------------")

def cicloRecursivo(lista):
    if len(lista) == 1:
        return lista[0]
    else:
        return lista[0] + cicloRecursivo(lista[1:])

elementos = [1,2,3,4,5,6,7,8,9,10,11,12]
resultado = cicloRecursivo(elementos)
print("La suma de los elementos de la lista es: ", resultado)



#2
#Escribe una función que combine 2 listas alternando los elementos de cada una. Ejemplo:  si se tienen las listas [a,b,c] y [1,2,3}, la funcion debería regresar [a,1,b,2,c,3].
print("-------------------------Alternar elmenetos de dos listas-------------------------")

def listasCombinadas():
    lista = ['a', 'b', 'c']
    lista2 = [1, 2, 3]
    print("Listas combinadas: ",(lista[0], lista2[0], lista[1], lista2[1], lista[2], lista2[2]))

listasCombinadas()


#3
#Escribe una función que calcule la lista de los primeros 100 números Fibonacci. Ejemplo 1 y 0, cada número subsecuente es la suma de los 2 anteriores.
print("-------------------------Los primeros 100 números Fibonacci---------------------------")
def primerosNumFib():
    a = 0
    b = 1
    print(a,b)

    for i in range(99):
        a, b = b, a + b
        print(b, end = ',')

primerosNumFib()

print()

#4
#Escribe una función que dada una lista de enteros no negativos, los organice de manera que formen en número más grande posible. Ejemplos: [50,2,1,9] el resultante seria: 95021. Verificar que la solución soporte este escenario [5,50,56] y el resultado seria : 56550.
print("-------------------------Número más grande formado por otra cantidad--------------------------------")
lista = []
def numeroMasGrande(elementos):
    for i in elementos:
        lista.append(int(i))
        lista.sort(reverse=True)
    print("El número más grande formado por estos dígidots es: ",(lista))
numeroIngresado1 = input("Escriba una cantidad NO NEGATIVO para añadir a la lista: ")
numeroIngresado2 = input("Escriba otra cantidad NO NEGATIVO para añadir a la lista: ")
numeroIngresado3 = input("Escriba una última cantidad NO NEGATIVO para añadir a la lista: ")
numeroMasGrande(numeroIngresado1 + numeroIngresado2 + numeroIngresado3)


#5
#5.- Usando los números del 1 al 9 en orden consecutivo, podemos realizar 3 operaciones entre cada digito: suma, resta o concatenación, para que el resultado de las operaciones conjuntas de como resultado 100. Por ejemplo: 1+2+34-5+67-8+9=100. Escribe un programa que encuentre todas las posibilidades de realizar esto.

print("-------------------------Todas las operaciones posibles que igualen a 100--------------------------------")
def sumaNumeros():
    cont= 1;
    s = ['','+','-']
    for a in s:
     for b in s:
      for c in s:
       for d in s:
        for e in s:
         for f in s:
          for g in s:
           for h in s:
             cadena = "1"+a+"2"+b+"3"+c+"4"+d+"5"+e+"6"+f+"7"+g+"8"+h+"9"
             if eval(cadena)==100:
               print("%i "%cont + cadena + " = " + "%i"%eval(cadena))
               cont = cont+1

sumaNumeros()















